var path = require("path");
var through = require("through2");

module.exports = function(params)
{
    var setPipe = through.obj(
        function(file, encoding, callback)
        {
            file.data.absoluteFilename = file.path;
            file.data.relativeFilename = path.relative(params.root, file.path);
            file.data.relativePath = file.data.relativeFilename.replace(/index.html$/, "");
            callback(null, file);
        });

    return setPipe;
}
